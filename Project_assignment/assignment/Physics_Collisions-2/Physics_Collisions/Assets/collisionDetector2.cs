﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionDetector2 : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D collisionEvent)
	{
		GetComponent<SpriteRenderer> ().color = Color.red;
	}

	void OnCollisionExit2D(Collision2D collisionEvent)
	{
		GetComponent<SpriteRenderer> ().color = Color.white;
	}
}
