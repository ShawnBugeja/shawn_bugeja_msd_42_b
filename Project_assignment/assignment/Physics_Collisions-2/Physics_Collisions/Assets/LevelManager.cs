﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	//public AudioSource sound;

	void Start()
	{
		//sound = GetComponent<AudioSource>();
	}

	public void LoadLevel(string levelName)
	{
		print("Loading level" + levelName);

		//loads the scene name level 
		SceneManager.LoadScene(levelName);
	}

	//this function is used to exit the game through the use of a button that will be linked in unity 
	public void exit()
	{
		print("Exiting level");
        Application.Quit();
    }

	public void LoadNextLevel()
	{
		//loads the next level in the build settings 
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

    public void BrickDestroyed()
    {
        //calling static variable breakableCount
        if (collisionDetector3D2.Count <= 0)
        {
            SceneManager.LoadScene("endScreen");
			//sound.Play ();
        }
    }
}
