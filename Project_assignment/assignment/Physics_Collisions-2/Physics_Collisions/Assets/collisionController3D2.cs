﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class collisionController3D2 : MonoBehaviour {
	GameObject blackSquare;
	public bool useForces;
    //GameObject sound;
	public AudioSource sound;

    // Use this for initialization
    void Start () {
		blackSquare = GameObject.Find ("Square1");
        //sound = GameObject.Find("bad_Hit");
		sound = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision boxCollision)
    {
        if (boxCollision.gameObject.name == "bad_tree")
        {
			
            //sound
            //sound.SetActive(true);

            transform.position = new Vector3(0f, -4f, 5f);
            collisionDetector3D2.Count = 0;
            SceneManager.LoadScene("game");
			Debug.Log ("Hit");
			sound.Play ();
			Debug.Log ("Hit2");
        }
    }

        // Update is called once per frame
        void Update () {

        if (blackSquare.transform.position.y >(-10f) ){
			blackSquare.GetComponent<Rigidbody> ().AddRelativeForce (Vector3.down * 10f);
		}
			
		if (useForces == false) {
			if (Input.GetKey (KeyCode.Space)) {
				blackSquare.transform.Translate (Vector3.up * 10f * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				blackSquare.transform.Translate (Vector3.left * 10f * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				blackSquare.transform.Translate (Vector3.right * 10f * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.UpArrow)) {
				blackSquare.transform.Translate (Vector3.forward * 10f * Time.deltaTime);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				blackSquare.transform.Translate (Vector3.back * 10f * Time.deltaTime);
			}
		}

        if (useForces == true)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                blackSquare.transform.Translate(Vector3.left * 10f * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                blackSquare.transform.Translate(Vector3.right * 10f * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                blackSquare.transform.Translate(Vector3.forward * 10f * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                blackSquare.transform.Translate(Vector3.back * 10f * Time.deltaTime);
            }
        }
    }

}
