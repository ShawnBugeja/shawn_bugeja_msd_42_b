﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionDetector : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D otherObject)
	{
		GetComponent<SpriteRenderer> ().color = Color.red;
	}

	void OnTriggerExit2D(Collider2D otherObject)
	{
		GetComponent<SpriteRenderer> ().color = Color.white;
	}
}
