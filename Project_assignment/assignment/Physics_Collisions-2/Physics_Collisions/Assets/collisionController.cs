﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionController : MonoBehaviour {
	GameObject sq1;

	public bool useForces;

	// Use this for initialization
	void Start () {
		
		sq1 = GameObject.Find ("Square1");


	}
	
	// Update is called once per frame
	void Update () {

		if (useForces == false) {
			//horizontal 
			sq1.transform.Translate
			(Vector3.right * Input.GetAxis ("Horizontal") * Time.deltaTime);
			//vertical
			sq1.transform.Translate
			(Vector3.up * Input.GetAxis ("Vertical") * Time.deltaTime);
		}

		if (useForces == true) {
			if (Input.GetKeyDown (KeyCode.UpArrow)) {
				sq1.GetComponent<Rigidbody2D> ().
						AddRelativeForce (new Vector3 (0f, 1f));
			}
			if (Input.GetKeyDown (KeyCode.DownArrow)) {
				sq1.GetComponent<Rigidbody2D> ().
						AddRelativeForce (new Vector3 (0f, -1f));
			}
			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				sq1.GetComponent<Rigidbody2D> ().
						AddRelativeForce (new Vector3 (-1f, 0f));
			}
			if (Input.GetKeyDown (KeyCode.RightArrow)) {
				sq1.GetComponent<Rigidbody2D> ().
						AddRelativeForce (new Vector3 (1f, 0f));
			}
		}
	}
}
