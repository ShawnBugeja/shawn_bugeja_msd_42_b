﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class collisionDetector3D2 : MonoBehaviour {
    
    public static int Score;
    private bool isBreakable = false;
    public static int Count = 0;
    private LevelManager myLevelManager;

    void OnCollisionEnter(Collision boxCollision)
    {
         if (boxCollision.gameObject.name == "Square1")
         {
            if (isBreakable == true) //if isBreakable == true
            {
                Count--;
                //move to method in LevelManager
                print(Count);
                myLevelManager.BrickDestroyed();
            }
        }

        GetComponent<Renderer>().material.color = new Color(0.5f, 0.7f, 0.5f, 0.5f);
        isBreakable = false;
    }

    void endScreen()
    {
        SceneManager.LoadScene("endScreen");
    }

	void OnCollisionExit(Collision boxCollision)
	{
		GetComponent<Renderer> ().material.color = new Color(0.5f,0.7f,0.5f,0.5f);
	}

    void Start()
    {
        myLevelManager = GameObject.FindObjectOfType<LevelManager>();

        if (this.tag == "good") {
            isBreakable = true;
        }
        //keep track of breakable tags
        if (isBreakable == true)
        {
            Count++;
        }


    }

}
