﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionController3D : MonoBehaviour {
	GameObject blackSquare;
	public bool useForces;

	// Use this for initialization
	void Start () {
		blackSquare = GameObject.Find ("Square1");	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.UpArrow)) {
			blackSquare.transform.Translate (Vector3.up * 10f * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			blackSquare.transform.Translate (Vector3.down * 10f * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.LeftArrow)) {
			blackSquare.transform.Translate (Vector3.left * 10f * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			blackSquare.transform.Translate (Vector3.right * 10f * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.Z)) {
			blackSquare.transform.Translate (Vector3.forward * 10f * Time.deltaTime);
		}
		if (Input.GetKey (KeyCode.X)) {
			blackSquare.transform.Translate (Vector3.back * 10f * Time.deltaTime);
		}

	}
}
